'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  // POST /api/users/register
  createUser: {
    body: {
      Name: _joi2.default.string().required(),
      Email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      PhoneNo: _joi2.default.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      userType: _joi2.default.string().required()
    }
  },
  reviews: {},
  sendOtp: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  findState: {
    body: {
      state: _joi2.default.string().required()
    }
  },

  findCity: {
    body: {
      city: _joi2.default.string().required()
    }
  }
};
module.exports = exports['default'];
//# sourceMappingURL=param-validation.js.map
