'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://localhost:27017/re-node-db',
  port: 4000,
  passportOptions: {
    session: false
  }
};
module.exports = exports['default'];
//# sourceMappingURL=test.js.map
