'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  env: 'development',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://localhost:27017/rajneta-dev',
  port: 6060,
  passportOptions: {
    session: false
  }
};
module.exports = exports['default'];
//# sourceMappingURL=development.js.map
