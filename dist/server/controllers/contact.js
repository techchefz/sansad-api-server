"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _contact = require("../models/contact");

var _contact2 = _interopRequireDefault(_contact);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function contact(req, res) {
    var returnObj = {
        success: false,
        message: "No contact found",
        data: {}
    };
    _contact2.default.find({ userId: req.body.userId }).populate("userId").then(function (dataFound) {
        if (dataFound.length != 0) {
            _contact2.default.findOneAndUpdate({ userId: req.body.userId }, {
                $set: {
                    "address": req.body.address,
                    "email": req.body.email,
                    "landlineNo": req.body.landlineNo,
                    "faxNo": req.body.faxNo,
                    "latitude": req.body.latitude,
                    "longitude": req.body.longitude
                }
            }, { new: true }).then(function (contactData) {
                if (contactData == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.data = contactData;
                    returnObj.message = "contact details updated successfully";
                    res.send(returnObj);
                }
            });
        } else {
            var newContact = new _contact2.default({
                address: req.body.address,
                email: req.body.email,
                landlineNo: req.body.landlineNo,
                faxNo: req.body.faxNo,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                userId: req.body.userId
            });
            newContact.save().then(function (contactData) {
                returnObj.success = true;
                returnObj.message = "contact details added successfully";
                returnObj.data = contactData;
                res.send(returnObj);
            });
        }
    });
}

function getContact(req, res) {
    var returnObj = {
        success: false,
        message: "No data found",
        data: {}
    };
    _contact2.default.find({ userId: req.headers.userid }).then(function (contactData) {
        if (contactData) {
            returnObj.success = true;
            returnObj.message = "data found";
            returnObj.data = contactData;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

exports.default = {
    contact: contact,
    getContact: getContact
};
module.exports = exports["default"];
//# sourceMappingURL=contact.js.map
