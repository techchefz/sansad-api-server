"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _featuredImage2 = require("../models/featuredImage");

var _featuredImage3 = _interopRequireDefault(_featuredImage2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function featuredImage(req, res) {
    var returnObj = {
        success: false,
        message: "No featured Image found",
        data: []
    };
    var gallery = [];
    req.files.forEach(function (pics) {
        gallery.push("http://139.59.71.216:6060/uploads/" + pics.filename);
    });
    _featuredImage3.default.find({ userId: req.body.userId }).then(function (dataFound) {
        if (dataFound.length != 0) {
            _featuredImage3.default.findOneAndUpdate({ userId: req.body.userId }, { $set: { "image": gallery } }, { new: true }).then(function (featuredImageData) {
                if (featuredImageData == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.message = "featuredImage Data updated successfully";
                    returnObj.data = featuredImageData;
                    res.send(returnObj);
                }
            });
        } else {
            var _featuredImage = new _featuredImage3.default({
                userId: req.body.userId,
                image: gallery
            });
            _featuredImage.save().then(function (featuredImageData) {
                returnObj.success = true;
                returnObj.message = "featured Image data added successfully";
                returnObj.data = featuredImageData;
                res.send(returnObj);
            });
        }
    });
}

function getFeaturedImage(req, res) {
    var returnObj = {
        success: false,
        message: "No featured Image found",
        data: []
    };
    var featuredImage = [];
    _featuredImage3.default.findOne({ userId: req.headers.userid }).then(function (dataFound) {
        featuredImage = dataFound.image;
        returnObj.success = true;
        returnObj.message = "Featured Image Found";
        returnObj.data = featuredImage;
        res.send(returnObj);
    });
}

exports.default = {
    featuredImage: featuredImage,
    getFeaturedImage: getFeaturedImage
};
module.exports = exports["default"];
//# sourceMappingURL=featuredImage.js.map
