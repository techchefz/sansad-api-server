"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _complaints = require("../models/complaints");

var _complaints2 = _interopRequireDefault(_complaints);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function complaints(req, res) {
    var returnObj = {
        success: false,
        message: "No complaint found",
        data: {}
    };
    var newComplaint = new _complaints2.default({
        name: req.body.name,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        complaint: req.body.complaint,
        userId: req.body.userId
    });
    newComplaint.save().then(function (complaints) {
        returnObj.success = true;
        returnObj.message = "New complaint saved";
        returnObj.data = complaints;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function dashboardComplaintData(req, res) {
    console.log('====================================');
    console.log("chala");
    console.log('====================================');
    var returnObj = {
        success: false,
        message: "No data found",
        data: {}
    };
    var todayDate = Date();
    var todayComplaints = 0;
    var totalComplaint = 0;
    var solvedComplaint = 0;
    var unsolvedComplaint = 0;
    _complaints2.default.find({ userId: req.headers.userid }).populate("userId").then(function (dataFound) {
        console.log('====================================');
        console.log(dataFound);
        console.log('====================================');
        if (dataFound) {
            // let tComplaint = dataFound.length;
            totalComplaint = dataFound.length;
            dataFound.forEach(function (dataFound) {
                if ((0, _moment2.default)(Date()).format("DD-MM-YYYY") == (0, _moment2.default)(dataFound.date).format("DD-MM-YYYY")) {
                    var tdayComplaint = [];
                    tdayComplaint.push(dataFound);
                    todayComplaints = tdayComplaint.length;
                }
                if (dataFound.status == true) {
                    solvedComplaint = solvedComplaint + 1;
                }
                if (dataFound.status == false) {
                    console.log('===============dataFound.status=====================');
                    console.log(dataFound.status);
                    console.log('====================================');
                    solvedComplaint = solvedComplaint + 1;
                }
            });
        } else {
            res.send(returnObj);
        }
    }).then(function () {
        returnObj.success = true;
        returnObj.message = "Data found";
        returnObj.data = {
            todayComplaints: todayComplaints,
            totalComplaint: totalComplaint,
            solvedComplaint: solvedComplaint,
            unsolvedComplaint: unsolvedComplaint
        };
        res.send(returnObj);
    });
}

function getComplaints(req, res) {
    var returnObj = {
        success: false,
        message: "No complaint found",
        data: {}
    };
    _complaints2.default.find({ userId: req.headers.userid }).populate("userId").then(function (foundData) {
        if (foundData) {
            returnObj.success = true;
            returnObj.message = "New poll saved";
            returnObj.data = foundData;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

exports.default = {
    complaints: complaints,
    getComplaints: getComplaints,
    dashboardComplaintData: dashboardComplaintData
};
module.exports = exports["default"];
//# sourceMappingURL=complaints.js.map
