"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _adminRegister = require("../models/adminRegister");

var _adminRegister2 = _interopRequireDefault(_adminRegister);

var _bcrypt = require("bcrypt");

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var saltRounds = 10;

function adminRegister(req, res) {
    var returnObj = {
        success: false,
        message: "User Already Exist`s",
        data: {}
    };
    _adminRegister2.default.findOne({ email: req.body.email }).then(function (foundUser) {
        if (foundUser != null) {
            res.send(returnObj);
        } else {
            _bcrypt2.default.hash(req.body.password, saltRounds).then(function (hashPassword) {
                var newUser = new _adminRegister2.default({
                    name: req.body.name,
                    phone: req.body.mobileNo,
                    email: req.body.email,
                    password: hashPassword
                });
                newUser.save().then(function (savedUser) {
                    returnObj.success = true;
                    returnObj.message = "Users Registered Successfully!";
                    returnObj.data = savedUser;
                    res.send(returnObj);
                });
            });
        }
    }).catch(function (error) {
        console.log('====================================');
        console.log(error);
        console.log('====================================');
    });
}

function fetchAdmin(req, res) {
    var returnObj = {
        success: false,
        message: "No user found",
        data: []
    };
    _adminRegister2.default.find().then(function (userFound) {
        returnObj.success = true;
        returnObj.message = "Users found";
        returnObj.data = userFound;
        res.send(returnObj);
    });
}

exports.default = {
    adminRegister: adminRegister,
    fetchAdmin: fetchAdmin
};
module.exports = exports["default"];
//# sourceMappingURL=adminRegister.js.map
