"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require("../models/config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function config(req, res) {
    var returnObj = {
        success: false,
        message: "data configuration failed",
        data: {}
    };
    _config2.default.find().then(function (data) {
        if (data.length != 0) {
            if (req.body.key == "googleApi") {
                _config2.default.findOneAndUpdate({ adminConfig: "admin" }, {
                    $set: {
                        gaId: req.body.gaId,
                        googleMapApi: req.body.googleMapApi
                    }
                }, { new: true }).then(function (configData) {
                    returnObj.success = true;
                    returnObj.message = "data configured successfully";
                    returnObj.data = configData;
                    res.send(returnObj);
                }).catch(function (error) {
                    returnObj.success = true;
                    returnObj.message = error;
                    res.send(returnObj);
                });
            } else if (req.body.key == "contactSettings") {
                _config2.default.findOneAndUpdate({ adminConfig: "admin" }, {
                    $set: {
                        title: req.body.title,
                        email: req.body.email,
                        mobile: req.body.mobile,
                        address: req.body.address,
                        website: req.body.website
                    }
                }, { new: true }).then(function (configData) {
                    returnObj.success = true;
                    returnObj.message = "data configured successfully";
                    returnObj.data = configData;
                    res.send(returnObj);
                }).catch(function (error) {
                    returnObj.success = true;
                    returnObj.message = error;
                    res.send(returnObj);
                });
            } else if (req.body.key == "socialMedia") {
                _config2.default.findOneAndUpdate({ adminConfig: "admin" }, {
                    $set: {
                        facebookUrl: req.body.facebookUrl,
                        twitterUrl: req.body.twitterUrl,
                        youtubeUrl: req.body.youtubeUrl,
                        instagramUrl: req.body.instagramUrl
                    }
                }, { new: true }).then(function (configData) {
                    returnObj.success = true;
                    returnObj.message = "data configured successfully";
                    returnObj.data = configData;
                    res.send(returnObj);
                }).catch(function (error) {
                    returnObj.success = true;
                    returnObj.message = error;
                    res.send(returnObj);
                });
            }
        } else {
            var newConfig = new _config2.default({
                gaId: req.body.gaId,
                googleMapApi: req.body.googleMapApi,
                title: req.body.title,
                email: req.body.email,
                mobile: req.body.mobile,
                address: req.body.address,
                website: req.body.website,
                facebookUrl: req.body.facebookUrl,
                twitterUrl: req.body.twitterUrl,
                youtubeUrl: req.body.youtubeUrl,
                instagramUrl: req.body.instagramUrl
            });
            newConfig.save().then(function (configData) {
                returnObj.success = true;
                returnObj.message = "data configured successfully";
                returnObj.data = configData;
                res.send(returnObj);
            }).catch(function (error) {
                console.log('==============error======================');
                console.log(error);
                console.log('==============error======================');
            });
        }
    });
}

function fetchConfig(req, res) {
    var returnObj = {
        success: false,
        message: "No configuration Found",
        data: []
    };
    var configData = [];
    _config2.default.find().then(function (data) {
        configData = data;
        returnObj.success = true;
        returnObj.message = "configuration data Found";
        returnObj.data = configData;
        res.send(returnObj);
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = "No configuration Found";
        res.send(returnObj);
    });
}

// 5b65880fb0cbf1f0df5ed9c0

exports.default = {
    config: config,
    fetchConfig: fetchConfig
};
module.exports = exports["default"];
//# sourceMappingURL=config.js.map
