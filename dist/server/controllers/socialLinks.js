"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _socialLinks = require("../models/socialLinks");

var _socialLinks2 = _interopRequireDefault(_socialLinks);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function socialLink(req, res) {
    var returnObj = {
        success: false,
        message: "No social Link found",
        data: {}
    };
    _socialLinks2.default.find({ userId: req.body.userId }).then(function (dataFound) {
        if (dataFound.length > 0) {
            _socialLinks2.default.findOneAndUpdate({ userId: req.body.userId }, {
                $set: {
                    "facebookLink": req.body.facebookLink,
                    "twitterLink": req.body.twitterLink,
                    "youtubeLink": req.body.youtubeLink,
                    "instagramLink": req.body.instagramLink,
                    "userId": req.body.userId
                }
            }, { new: true }).then(function (socialData) {
                if (socialData == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.data = socialData;
                    returnObj.message = "social details updated successfully";
                    res.send(returnObj);
                }
            });
        } else {
            var newSocialLink = new _socialLinks2.default({
                facebookLink: req.body.facebookLink,
                twitterLink: req.body.twitterLink,
                youtubeLink: req.body.youtubeLink,
                instagramLink: req.body.instagramLink,
                userId: req.body.userId
            });
            newSocialLink.save().then(function (socialData) {
                returnObj.success = true;
                returnObj.message = "social details added successfully";
                returnObj.data = socialData;
                res.send(returnObj);
            });
        }
    });
}

function getsocialLink(req, res) {
    var returnObj = {
        success: false,
        message: "No data found",
        data: {}
    };
    _socialLinks2.default.find({ userId: req.headers.userid }).populate("userId").then(function (socialData) {
        if (socialData) {
            returnObj.success = true;
            returnObj.message = "data found";
            returnObj.data = socialData;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

exports.default = {
    socialLink: socialLink,
    getsocialLink: getsocialLink
};
module.exports = exports["default"];
//# sourceMappingURL=socialLinks.js.map
