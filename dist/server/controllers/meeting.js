"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _meeting = require("../models/meeting");

var _meeting2 = _interopRequireDefault(_meeting);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function meeting(req, res) {
    var returnObj = {
        success: false,
        message: "No meeting found",
        data: {}
    };
    var newMeeting = new _meeting2.default({
        userId: req.body.userId,
        venueName: req.body.venueName,
        venueDateTime: req.body.venueDateTime
    });
    newMeeting.save().then(function (meetingData) {
        returnObj.success = true;
        returnObj.message = "New meeting saved";
        returnObj.data = meetingData;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function fetchMeeting(req, res) {
    var returnObj = {
        success: false,
        message: "No meeting Found",
        data: []
    };
    var meetingData = [];
    _meeting2.default.find({ userId: req.headers.userid }).populate("userId").then(function (data) {
        if (data) {
            meetingData = data;
            returnObj.success = true;
            returnObj.message = "meeting data Found";
            returnObj.data = meetingData;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = "No meeting Found";
        res.send(returnObj);
    });
}
exports.default = {
    meeting: meeting,
    fetchMeeting: fetchMeeting
};
module.exports = exports["default"];
//# sourceMappingURL=meeting.js.map
