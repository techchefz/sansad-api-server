"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _user = require("../models/user");

var _user2 = _interopRequireDefault(_user);

var _adminRegister = require("../models/adminRegister");

var _adminRegister2 = _interopRequireDefault(_adminRegister);

var _superAdmin = require("../models/superAdmin");

var _superAdmin2 = _interopRequireDefault(_superAdmin);

var _bcrypt = require("bcrypt");

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var saltRounds = 10;

function superAdminLogin(req, res) {
    var returnObj = {
        success: false,
        message: "User login failed",
        jwtAccessToken: null,
        data: {}
    };
    var jwtAccessToken = null;
    _superAdmin2.default.findOne({ email: req.body.email }).then(function (foundUser) {
        if (foundUser != null) {
            _bcrypt2.default.compare(req.body.password, foundUser.password, function (err, result) {
                if (result) {
                    jwtAccessToken = _jsonwebtoken2.default.sign(foundUser, _env2.default.jwtSecret);
                    returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                    returnObj.success = true;
                    returnObj.message = "Authentication Successful";
                    delete foundUser.password;
                    returnObj.data = foundUser;
                    returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                    res.send(returnObj);
                } else {
                    res.send(returnObj);
                }
            });
        }
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = error;
        res.send(returnObj);
    });
}

function adminLogin(req, res) {
    var returnObj = {
        success: false,
        message: "User login failed",
        jwtAccessToken: null,
        data: {}
    };
    var jwtAccessToken = null;
    _adminRegister2.default.findOne({ email: req.body.email }).then(function (foundUser) {
        if (foundUser != null) {
            _bcrypt2.default.compare(req.body.password, foundUser.password, function (err, result) {
                if (result) {
                    jwtAccessToken = _jsonwebtoken2.default.sign(foundUser, _env2.default.jwtSecret);
                    returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                    returnObj.success = true;
                    returnObj.message = "Authentication Successful";
                    delete foundUser.password;
                    returnObj.data = foundUser;
                    returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                    res.send(returnObj);
                } else {
                    res.send(returnObj);
                }
            });
        }
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = error;
        res.send(returnObj);
    });
}

function login(req, res) {
    var returnObj = {
        success: false,
        message: "User auth failed",
        jwtAccessToken: null,
        data: {}
    };
    var jwtAccessToken = null;
    var userId = req.body.userId;
    _user2.default.findOne({ email: req.body.email }).then(function (foundUser) {
        if (foundUser != null) {
            _bcrypt2.default.compare(req.body.password, foundUser.password, function (err, result) {
                if (result) {
                    if (foundUser.userId == req.body.userId) {
                        jwtAccessToken = _jsonwebtoken2.default.sign(foundUser, _env2.default.jwtSecret);
                        returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                        returnObj.success = true;
                        returnObj.message = "Authentication Successful";
                        delete foundUser.password;
                        returnObj.data = foundUser;
                        returnObj.jwtAccessToken = "JWT " + jwtAccessToken;
                        res.send(returnObj);
                    } else {
                        res.send(returnObj);
                    }
                } else {
                    res.send(returnObj);
                }
            });
        } else {
            res.send(returnObj);
        }
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = error;
        res.send(returnObj);
    });
}

function changePassword(req, res) {
    var returnObj = {
        success: false,
        message: "password not changed",
        data: {}
    };
    var HashPassword = null;
    if (req.body.password != null) {
        _bcrypt2.default.hash(req.body.password, saltRounds).then(function (hashPassword) {
            _user2.default.findOneAndUpdate({ email: req.body.email }, { $set: { password: hashPassword } }, { new: true }).then(function (passwordChanged) {
                if (passwordChanged == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.message = "password changed successfully";
                    res.send(returnObj);
                }
            });
        });
    }
}

exports.default = {
    login: login,
    changePassword: changePassword,
    adminLogin: adminLogin,
    superAdminLogin: superAdminLogin
};
module.exports = exports["default"];
//# sourceMappingURL=auth.js.map
