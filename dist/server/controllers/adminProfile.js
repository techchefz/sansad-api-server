"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _profile = require("../models/profile");

var _profile2 = _interopRequireDefault(_profile);

var _user = require("../models/user");

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function profile(req, res) {
    var returnObj = {
        success: false,
        message: "No profile found",
        data: {}
    };
    _profile2.default.findOne({ userId: req.body.userId }).populate("userId").then(function (dataFound) {
        if (dataFound) {
            _profile2.default.findOneAndUpdate({ userId: req.body.userId }, {
                $set: {
                    "name": req.body.name,
                    "dateOfBirth": req.body.dateOfBirth,
                    "qualification": req.body.qualification,
                    "profilePicture": req.files !== undefined ? "http://139.59.71.216:6060/uploads/" + req.file.filename : dataFound.profilePicture,
                    "about": req.body.about
                }
            }, { new: true }).then(function (profileData) {
                if (profileData == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.message = "journey data updated successfully";
                    returnObj.data = profileData;
                    res.send(returnObj);
                }
            });
        } else {
            var newProfile = new _profile2.default({
                name: req.body.name,
                dateOfBirth: req.body.dateOfBirth,
                qualification: req.body.qualification,
                profilePicture: "http://139.59.71.216:6060/uploads/" + req.file.filename,
                about: req.body.about,
                userId: req.body.userId
            });
            newProfile.save().then(function (profileData) {
                returnObj.success = true;
                returnObj.message = "New profile saved";
                returnObj.data = profileData;
                res.send(returnObj);
            }).catch(function (error) {
                console.log(error);
            });
        }
    });
}

function getProfile(req, res) {
    var returnObj = {
        success: false,
        message: "No Profile Found",
        data: null
    };
    var profile = [];
    _profile2.default.find({ userId: req.headers.userid }).populate("userId").then(function (foundProfile) {
        if (foundProfile) {
            profile = foundProfile;
            returnObj.success = true;
            returnObj.message = "Profile Found";
            returnObj.data = profile;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    }).catch(function (error) {
        console.log(error);
    });
}

exports.default = {
    profile: profile,
    getProfile: getProfile
};
module.exports = exports["default"];
//# sourceMappingURL=adminProfile.js.map
