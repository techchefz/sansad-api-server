"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _enquiries = require("../models/enquiries");

var _enquiries2 = _interopRequireDefault(_enquiries);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function enquires(req, res) {
    var returnObj = {
        success: false,
        message: "No enquiry found",
        data: {}
    };
    var newEnquiry = new _enquiries2.default({
        name: req.body.name,
        mobile: req.body.mobile,
        email: req.body.email,
        query: req.body.query,
        pageName: req.body.pageName,
        pageURL: req.body.pageURL
    });
    newEnquiry.save().then(function (enquiryData) {
        returnObj.success = true;
        returnObj.message = "New query saved";
        returnObj.data = enquiryData;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function getEnquires(req, res) {
    var returnObj = {
        success: false,
        message: "No Enquery Found",
        data: []
    };

    var enquery = [];
    _enquiries2.default.find().then(function (foundEnquery) {
        enquery = foundEnquery;
        returnObj.success = true;
        returnObj.message = "Enquery Found";
        returnObj.data = enquery;
        res.send(returnObj);
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = "No Enquery Found";
        res.send(returnObj);
    });
}

exports.default = {
    enquires: enquires,
    getEnquires: getEnquires
};
module.exports = exports["default"];
//# sourceMappingURL=enquiries.js.map
