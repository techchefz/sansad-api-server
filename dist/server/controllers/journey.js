"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _journey = require("../models/journey");

var _journey2 = _interopRequireDefault(_journey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function journey(req, res) {
    var returnObj = {
        success: false,
        message: "No journey found",
        data: {}
    };
    _journey2.default.find({ userId: req.body.userId }).then(function (dataFound) {
        if (dataFound.length != 0) {
            _journey2.default.findOneAndUpdate({ userId: req.body.userId }, {
                $set: {
                    "journey": req.body.journey,
                    "image": req.file !== undefined ? "http://139.59.71.216:6060/uploads/" + req.file.filename : dataFound.image
                }
            }, { new: true }).then(function (journeyData) {
                if (journeyData == null) {
                    res.send(returnObj);
                } else {
                    returnObj.success = true;
                    returnObj.message = "journey data updated successfully";
                    res.send(returnObj);
                }
            });
        } else {
            var newJourney = new _journey2.default({
                journey: req.body.journey,
                image: "http://139.59.71.216:6060/uploads/" + req.file.filename,
                userId: req.body.userId
            });
            newJourney.save().then(function (journeyData) {
                returnObj.success = true;
                returnObj.message = "journey data added successfully";
                returnObj.data = journeyData;
                res.send(returnObj);
            });
        }
    });
}

function getJourney(req, res) {
    var returnObj = {
        success: false,
        message: "No journey found",
        data: {}
    };
    _journey2.default.find({ userId: req.headers.userid }).populate("userId").then(function (journeyData) {
        if (journeyData) {
            returnObj.success = true;
            returnObj.message = "journey data found";
            returnObj.data = journeyData;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

exports.default = {
    journey: journey,
    getJourney: getJourney
};
module.exports = exports["default"];
//# sourceMappingURL=journey.js.map
