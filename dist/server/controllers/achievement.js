"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _achievement = require("../models/achievement");

var _achievement2 = _interopRequireDefault(_achievement);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function achievement(req, res) {
  var returnObj = {
    success: false,
    message: "No achievement found",
    data: {}
  };

  var achievement = JSON.parse(req.body.achievement);

  _achievement2.default.find({ userId: req.body.userId }).then(function (dataFound) {
    if (dataFound.length != 0) {
      _achievement2.default.findOneAndUpdate({ userId: req.body.userId }, {
        $set: {
          achievement: achievement,
          image: req.file !== undefined ? "http://139.59.71.216:6060/uploads/" + req.file.filename : dataFound.image
        }
      }, { new: true }).then(function (achievementData) {
        if (achievementData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.message = "Achievement data updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      var newAchievement = new _achievement2.default({
        achievement: achievement,
        image: "http://139.59.71.216:6060/uploads/" + req.file.filename,
        userId: req.body.userId
      });
      newAchievement.save().then(function (achievementData) {
        returnObj.success = true;
        returnObj.message = "Achievement data added successfully";
        returnObj.data = achievementData;
        res.send(returnObj);
      });
    }
  });
}

function getAchievement(req, res) {
  var returnObj = {
    success: false,
    message: "No achievement found",
    data: {}
  };
  _achievement2.default.find({ userId: req.headers.userid }).populate("userId").then(function (achievementData) {
    if (achievementData) {
      returnObj.success = true;
      returnObj.message = "achievement data found";
      returnObj.data = achievementData;
      res.send(returnObj);
    } else {
      res.send(returnObj);
    }
  });
}

exports.default = {
  achievement: achievement,
  getAchievement: getAchievement
};
module.exports = exports["default"];
//# sourceMappingURL=achievement.js.map
