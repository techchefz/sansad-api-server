"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _user = require("../models/user");

var _user2 = _interopRequireDefault(_user);

var _bcrypt = require("bcrypt");

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var saltRounds = 10;

function createUser(req, res) {
    var returnObj = {
        success: false,
        message: "User Already Exist`s",
        data: {}
    };
    console.log('====================================');
    console.log(req.body);
    console.log('====================================');
    _user2.default.findOne({ email: req.body.email }).then(function (foundUser) {
        if (foundUser != null) {
            res.send(returnObj);
        } else {
            _bcrypt2.default.hash(req.body.password, saltRounds).then(function (hashPassword) {
                var newUser = new _user2.default({
                    name: req.body.name,
                    phone: req.body.mobileNo,
                    email: req.body.email,
                    password: hashPassword,
                    userType: req.body.userType,
                    userId: req.body.userId
                });
                newUser.save().then(function (savedUser) {
                    returnObj.success = true;
                    returnObj.message = "Users Registered Successfully!";
                    returnObj.data = savedUser;
                    res.send(returnObj);
                });
            });
        }
    }).catch(function (error) {
        console.log('====================================');
        console.log(error);
        console.log('====================================');
    });
}

function fetchUser(req, res) {
    var returnObj = {
        success: false,
        message: "No user found",
        data: []
    };
    _user2.default.find({ userId: req.headers.userid }).then(function (userFound) {
        if (userFound == null) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "Users found";
            returnObj.data = userFound;
            res.send(returnObj);
        }
    });
}

function fetchAllUsers(req, res) {
    var returnObj = {
        success: false,
        message: "No user found",
        data: []
    };
    _user2.default.find().then(function (userFound) {
        if (userFound == null) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "Users found";
            returnObj.data = userFound;
            res.send(returnObj);
        }
    });
}

function review(req, res) {
    var returnObj = {
        success: false,
        message: "User review error",
        data: {}
    };

    var newReview = new ReviewSchema({
        name: req.body.name,
        page: req.body.page,
        pageUrl: req.body.pageUrl,
        email: req.body.email,
        subject: req.body.subject,
        rating: req.body.rating,
        message: req.body.message
    });
    newReview.save().then(function (userReview) {
        returnObj.success = true;
        returnObj.message = "User review saved";
        returnObj.data = userReview;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function getAllReviews(req, res) {

    var returnObj = {
        success: false,
        message: "No Review Found",
        data: []
    };

    var reviews = [];
    ReviewSchema.find().then(function (foundItems) {
        reviews = foundItems;
        returnObj.success = true;
        returnObj.message = "Review Found";
        returnObj.data = reviews;
        res.send(returnObj);
    }).catch(function (error) {
        returnObj.success = false;
        returnObj.message = "Reviews not Found";
        res.send(returnObj);
    });
}

function reviewStatusUpdate(req, res) {
    var returnObj = {
        success: false,
        message: "Status not updated",
        data: {}
    };
    ReviewSchema.findOneAndUpdate({ _id: req.body._id }, { $set: { "status": req.body.status } }, { new: true }).then(function (statusUpdate) {
        if (statusUpdate == null) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "status updated successfully";
            res.send(returnObj);
        }
    });
}

function reviewDelete(req, res) {
    var returnObj = {
        success: false,
        message: "No review found",
        data: {}
    };
    ReviewSchema.findOneAndRemove({ _id: req.body._id }).then(function (reviewRemoved) {
        if (reviewRemoved == null) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "Review deleted successfully";
            res.send(returnObj);
        }
    });
}

function Itinerary(req, res) {
    var returnObj = {
        success: false,
        message: "No data added",
        data: {}
    };
    var newItinerary = new ItinerarySchema({
        pageTitle: req.body.pageTitle,
        metaTitle: req.body.metaTitle,
        pageUrl: req.body.pageUrl,
        metaDescription: req.body.metaDescription,
        metaKeywords: req.body.metaKeywords,
        description: req.body.description,
        imagePath: "http://localhost:3000/uploads/" + req.file.filename,
        itineraryDays: req.body.itineraryDays,
        tripDetail: req.body.tripDetail
    });
    newItinerary.save().then(function (ItineraryData) {
        returnObj.success = true;
        returnObj.message = "Data added";
        returnObj.data = ItineraryData;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function fetchUserMonthlyCount(req, res) {
    var returnObj = {
        success: false,
        message: "No Data found",
        data: []
    };
    var Data = null;
    var Jan = 0;
    var Feb = 0;
    var Mar = 0;
    var Apr = 0;
    var May = 0;
    var Jun = 0;
    var Jul = 0;
    var Aug = 0;
    var Sept = 0;
    var Oct = 0;
    var Nov = 0;
    var Dec = 0;
    var currentYear = (0, _moment2.default)(Date()).format("YY");
    _user2.default.find().then(function (data) {
        data.forEach(function (allData) {
            if (currentYear == (0, _moment2.default)(allData.date).format("YY")) {
                if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("1").format("MMM")) {
                    Jan = Jan + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("2").format("MMM")) {
                    Feb = Feb + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("3").format("MMM")) {
                    Mar = Mar + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("4").format("MMM")) {
                    Apr = Apr + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("5").format("MMM")) {
                    May = May + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("6").format("MMM")) {
                    Jun = Jun + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("7").format("MMM")) {
                    Jul = Jul + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("8").format("MMM")) {
                    Aug = Aug + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("9").format("MMM")) {
                    Sept = Sept + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("10").format("MMM")) {
                    Oct = Oct + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("11").format("MMM")) {
                    Nov = Nov + 1;
                } else if ((0, _moment2.default)(allData.date).format("MMM") == (0, _moment2.default)("12").format("MMM")) {
                    Dec = Dec + 1;
                }
            }
        });
        Data = {
            Jan: Jan,
            Feb: Feb,
            Mar: Mar,
            Apr: Apr,
            May: May,
            Jun: Jun,
            Jul: Jul,
            Aug: Aug,
            Sept: Sept,
            Oct: Oct,
            Nov: Nov,
            Dec: Dec
        };
    }).then(function () {
        returnObj.success = true;
        returnObj.message = "Monthly Reviews count";
        returnObj.data = Data;
        res.send(returnObj);
    });
}

exports.default = {
    createUser: createUser,
    fetchUser: fetchUser,
    fetchAllUsers: fetchAllUsers,
    review: review,
    Itinerary: Itinerary,
    reviewStatusUpdate: reviewStatusUpdate,
    getAllReviews: getAllReviews,
    reviewDelete: reviewDelete,
    fetchUserMonthlyCount: fetchUserMonthlyCount
};
module.exports = exports["default"];
//# sourceMappingURL=user.js.map
