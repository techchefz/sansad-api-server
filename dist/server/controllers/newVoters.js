"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _newVoters = require("../models/newVoters");

var _newVoters2 = _interopRequireDefault(_newVoters);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function newVoters(req, res) {
	var returnObj = {
		success: false,
		message: "No voter found",
		data: {}
	};
	var newVoter = new _newVoters2.default({
		name: req.body.name,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		userId: req.body.userId
	});
	newVoter.save().then(function (voters) {
		returnObj.success = true;
		returnObj.message = "New voter saved";
		returnObj.data = voters;
		res.send(returnObj);
	}).catch(function (error) {
		console.log('==============error======================');
		console.log(error);
		console.log('==============error======================');
	});
}

// function dashboardComplaintData(req, res) {
//     console.log('====================================');
//     console.log("chala");
//     console.log('====================================');
//     let returnObj = {
//         success: false,
//         message: "No data found",
//         data: {}
//     }
//     let todayDate = Date();
//     let todayComplaints = 0;
//     let totalComplaint = 0;
//     let solvedComplaint = 0;
//     let unsolvedComplaint = 0;
//     VotersSchema.find({ userId: req.headers.userid }).populate("userId")
//         .then(dataFound => {
//             console.log('====================================');
//             console.log(dataFound);
//             console.log('====================================');
//             if (dataFound) {
//                 // let tComplaint = dataFound.length;
//                 totalComplaint = dataFound.length;
//                 dataFound.forEach((dataFound) => {
//                     if (moment(Date()).format("DD-MM-YYYY") == moment(dataFound.date).format("DD-MM-YYYY")) {
//                         let tdayComplaint = []
//                         tdayComplaint.push(dataFound);
//                         todayComplaints = tdayComplaint.length;
//                     }
//                     if (dataFound.status == true) {
//                         solvedComplaint = solvedComplaint + 1;
//                     }
//                     if (dataFound.status == false) {
//                         console.log('===============dataFound.status=====================');
//                         console.log(dataFound.status);
//                         console.log('====================================');
//                         solvedComplaint = solvedComplaint + 1;
//                     }
//                 })
//             } else {
//                 res.send(returnObj)
//             }
//         }).then(() => {
//             returnObj.success = true;
//             returnObj.message = "Data found"
//             returnObj.data = {
//                 todayComplaints,
//                 totalComplaint,
//                 solvedComplaint,
//                 unsolvedComplaint
//             }
//             res.send(returnObj)
//         })
// }

function getVoters(req, res) {
	var returnObj = {
		success: false,
		message: "No voter found",
		data: {}
	};
	_newVoters2.default.find({ userId: req.headers.userid }).populate("userId").then(function (foundData) {
		if (foundData) {
			returnObj.success = true;
			returnObj.message = "data found";
			returnObj.data = foundData;
			res.send(returnObj);
		} else {
			res.send(returnObj);
		}
	});
}

exports.default = {
	newVoters: newVoters,
	getVoters: getVoters
	// dashboardComplaintData
};
module.exports = exports["default"];
//# sourceMappingURL=newVoters.js.map
