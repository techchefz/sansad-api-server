"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _socialFeeds = require("../models/socialFeeds");

var _socialFeeds2 = _interopRequireDefault(_socialFeeds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function socialFeedsLink(req, res) {
  var returnObj = {
    success: false,
    message: "No social Link found",
    data: {}
  };
  _socialFeeds2.default.find({ userId: req.body.userId }).then(function (dataFound) {
    if (dataFound.length > 0) {
      _socialFeeds2.default.findOneAndUpdate({ userId: req.body.userId }, {
        $set: {
          facebook: req.body.facebook,
          consumer_key: req.body.consumer_key,
          consumer_secret: req.body.consumer_secret,
          access_token_key: req.body.access_token_key,
          access_token_secret: req.body.access_token_secret,
          youtube: req.body.youtube,
          instagram: req.body.instagram,
          userId: req.body.userId
        }
      }, { new: true }).then(function (socialData) {
        if (socialData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.data = socialData;
          returnObj.message = "social details updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      var newSocial = new _socialFeeds2.default({
        facebook: req.body.facebook,
        consumer_key: req.body.consumer_key,
        consumer_secret: req.body.consumer_secret,
        access_token_key: req.body.access_token_key,
        access_token_secret: req.body.access_token_secret,
        youtube: req.body.youtube,
        instagram: req.body.instagram,
        userId: req.body.userId
      });
      newSocial.save().then(function (socialData) {
        returnObj.success = true;
        returnObj.message = "social details added successfully";
        returnObj.data = socialData;
        res.send(returnObj);
      });
    }
  });
}

function getsocialFeedsLink(req, res) {
  var returnObj = {
    success: false,
    message: "No data found",
    data: {}
  };
  _socialFeeds2.default.findOne({ userId: req.headers.userid }).populate("userId").then(function (socialData) {
    if (socialData) {
      returnObj.success = true;
      returnObj.message = "data found";
      returnObj.data = socialData;
      res.send(returnObj);
    } else {
      res.send(returnObj);
    }
  });
}

exports.default = {
  socialFeedsLink: socialFeedsLink,
  getsocialFeedsLink: getsocialFeedsLink
};
module.exports = exports["default"];
//# sourceMappingURL=socialFeed.js.map
