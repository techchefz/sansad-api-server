"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _poll = require("../models/poll");

var _poll2 = _interopRequireDefault(_poll);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function poll(req, res) {
    var returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    };
    var newPoll = new _poll2.default({
        question: req.body.question,
        options: req.body.options,
        userId: req.body.userId
    });
    newPoll.save().then(function (pollData) {
        returnObj.success = true;
        returnObj.message = "New poll saved";
        returnObj.data = pollData;
        res.send(returnObj);
    }).catch(function (error) {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function getPoll(req, res) {
    var returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    };
    console.log('====================================');
    console.log(req.headers);
    console.log('====================================');
    _poll2.default.find({ userId: req.headers.userid }).populate("userId").then(function (foundPolls) {
        if (foundPolls) {
            returnObj.success = true;
            returnObj.message = "poll found";
            returnObj.data = foundPolls;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

function fetchPoll(req, res) {
    var returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    };
    _poll2.default.find({ userId: req.headers.userid }).populate("userId").then(function (foundPolls) {
        if (foundPolls) {
            foundPolls.forEach(function (data) {
                if (data.status == true) {
                    returnObj.success = true;
                    returnObj.message = "poll found";
                    returnObj.data = data;
                    res.send(returnObj);
                } else {
                    // res.send(returnObj)
                }
            });
        } else {
            res.send(returnObj);
        }
    });
}

function pollStatus(req, res) {
    var returnObj = {
        success: false,
        message: "status updation failed",
        data: {}
    };
    var userID = req.body.userId;
    _poll2.default.update({ userId: req.body.userId }, { $set: { status: false } }, { multi: true }).then(function (data) {
        console.log('====================================');
        console.log(data);
        console.log('====================================');
        _poll2.default.findOneAndUpdate({ _id: req.body._id }, {
            $set: {
                status: req.body.status
            }
        }, { new: true }).then(function (foundPolls) {
            returnObj.success = true;
            returnObj.message = "status updated successfully";
            returnObj.data = foundPolls;
            res.send(returnObj);
        });
    });
}

function saveResponse(req, res) {
    var returnObj = {
        success: false,
        message: "Response Updation Failed",
        data: {}
    };
    var optionA = null;
    var optionB = null;
    var optionC = null;
    var optionD = null;
    var users = [];
    console.log('====================================');
    console.log(req.body);
    console.log('====================================');
    _poll2.default.find({ _id: req.body._id }).then(function (pollFound) {
        console.log('====================================');
        console.log(req.body);
        console.log('====================================');
        pollFound.forEach(function (data) {
            if (req.body.response == "a") {
                optionA = data.responseA;
                _poll2.default.findOneAndUpdate({ _id: req.body._id }, { $set: { responseA: optionA + 1 } }, { new: true });
                optionA = data.responseA;
                _poll2.default.findOneAndUpdate({ _id: req.body._id }, { $set: { responseA: optionA + 1 } }, { new: true }).then(function (foundPoll) {
                    returnObj.message = "poll found";
                    returnObj.data = foundPoll;
                    res.send(returnObj);
                });
            } else if (req.body.response == "b") {
                optionB = data.responseB;
                _poll2.default.findOneAndUpdate({ _id: req.body._id }, { $set: { responseB: optionB + 1 } }, { new: true }).then(function (foundPoll) {
                    returnObj.message = "poll found";
                    returnObj.data = foundPoll;
                    res.send(returnObj);
                });
            } else if (req.body.response == "c") {
                optionC = data.responseC;
                _poll2.default.findOneAndUpdate({ _id: req.body._id }, { $set: { responseC: optionC + 1 } }, { new: true }).then(function (foundPoll) {
                    returnObj.message = "poll found";
                    returnObj.data = foundPoll;
                    res.send(returnObj);
                });
            } else if (req.body.response == "d") {
                optionD = data.responseD;
                _poll2.default.findOneAndUpdate({ _id: req.body._id }, { $set: { responseD: optionD + 1 } }, { new: true }).then(function (foundPoll) {
                    returnObj.message = "poll found";
                    returnObj.data = foundPoll;
                    res.send(returnObj);
                });
            }
        });
    });
}
exports.default = {
    poll: poll,
    getPoll: getPoll,
    fetchPoll: fetchPoll,
    saveResponse: saveResponse,
    pollStatus: pollStatus
};
module.exports = exports["default"];
//# sourceMappingURL=poll.js.map
