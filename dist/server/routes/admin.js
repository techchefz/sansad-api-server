"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require("express-validation");

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require("http-status");

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _paramValidation = require("../../config/param-validation");

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _APIError = require("../helpers/APIError");

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

var _multer = require("multer");

var _multer2 = _interopRequireDefault(_multer);

var _user = require("../controllers/user");

var _user2 = _interopRequireDefault(_user);

var _adminProfile = require("../controllers/adminProfile");

var _adminProfile2 = _interopRequireDefault(_adminProfile);

var _poll = require("../controllers/poll");

var _poll2 = _interopRequireDefault(_poll);

var _journey = require("../controllers/journey");

var _journey2 = _interopRequireDefault(_journey);

var _complaints = require("../controllers/complaints");

var _complaints2 = _interopRequireDefault(_complaints);

var _contact = require("../controllers/contact");

var _contact2 = _interopRequireDefault(_contact);

var _config = require("../controllers/config");

var _config2 = _interopRequireDefault(_config);

var _socialLinks = require("../controllers/socialLinks");

var _socialLinks2 = _interopRequireDefault(_socialLinks);

var _meeting = require("../controllers/meeting");

var _meeting2 = _interopRequireDefault(_meeting);

var _featuredImage = require("../controllers/featuredImage");

var _featuredImage2 = _interopRequireDefault(_featuredImage);

var _adminRegister = require("../controllers/adminRegister");

var _adminRegister2 = _interopRequireDefault(_adminRegister);

var _achievement = require("../controllers/achievement");

var _achievement2 = _interopRequireDefault(_achievement);

var _socialFeed = require("../controllers/socialFeed");

var _socialFeed2 = _interopRequireDefault(_socialFeed);

var _newVoters = require("../controllers/newVoters");

var _newVoters2 = _interopRequireDefault(_newVoters);

var _twitter = require("../controllers/twitter");

var _twitter2 = _interopRequireDefault(_twitter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

var storage = _multer2.default.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function filename(req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = "./public/uploads" + imageName;
  }
});
var uploadObj = (0, _multer2.default)({ storage: storage });

router.post("/adminRegister", _adminRegister2.default.adminRegister);

router.get("/adminRegister/fetchAdmin", _adminRegister2.default.fetchAdmin);

router.post("/register", _user2.default.createUser);

router.route("/adminProfile").post(uploadObj.single("image"), _adminProfile2.default.profile);

router.post("/poll", _poll2.default.poll);

router.post("/pollStatus", _poll2.default.pollStatus);

router.route("/journey").post(uploadObj.single("image"), _journey2.default.journey);

router.route("/achievement").post(uploadObj.single("image"), _achievement2.default.achievement);

router.post("/complaints", _complaints2.default.complaints);

router.post("/contact", _contact2.default.contact);

router.post("/socialLink", _socialLinks2.default.socialLink);

router.post("/socialFeedsLink", _socialFeed2.default.socialFeedsLink);

router.post("/newVoters", _newVoters2.default.newVoters);

router.post("/reviewDelete", _user2.default.reviewDelete);

router.post("/fetchConfig", _config2.default.fetchConfig);

router.get("/dashboardComplaintData", _complaints2.default.dashboardComplaintData);

router.post('/twitterFeeds', _twitter2.default.twitterFeeds);

router.post('/youtubeId', _twitter2.default.youtubeId);

router.post("/meeting", _meeting2.default.meeting);

router.route("/featuredImage").post(uploadObj.array("image"), _featuredImage2.default.featuredImage);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use(function (req, res, next) {
  _passport2.default.authenticate("jwt", _env2.default.passportOptions, function (error, userDtls, info) {
    //eslint-disable-line
    if (error) {
      var err = new _APIError2.default("token not matched", _httpStatus2.default.UNAUTHORIZED);
      return next(err);
    } else if (userDtls) {
      req.user = userDtls;
      next();
    } else {
      var _err = new _APIError2.default("token not matched and error msg " + info, _httpStatus2.default.UNAUTHORIZED);
      return next(_err);
    }
  })(req, res, next);
});

exports.default = router;
module.exports = exports["default"];
//# sourceMappingURL=admin.js.map
