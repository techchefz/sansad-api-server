"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require("express-validation");

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require("http-status");

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require("../helpers/APIError");

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require("../../config/param-validation");

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _user = require("../controllers/user");

var _user2 = _interopRequireDefault(_user);

var _adminProfile = require("../controllers/adminProfile");

var _adminProfile2 = _interopRequireDefault(_adminProfile);

var _poll = require("../controllers/poll");

var _poll2 = _interopRequireDefault(_poll);

var _journey = require("../controllers/journey");

var _journey2 = _interopRequireDefault(_journey);

var _complaints = require("../controllers/complaints");

var _complaints2 = _interopRequireDefault(_complaints);

var _contact = require("../controllers/contact");

var _contact2 = _interopRequireDefault(_contact);

var _socialLinks = require("../controllers/socialLinks");

var _socialLinks2 = _interopRequireDefault(_socialLinks);

var _meeting = require("../controllers/meeting");

var _meeting2 = _interopRequireDefault(_meeting);

var _featuredImage = require("../controllers/featuredImage");

var _featuredImage2 = _interopRequireDefault(_featuredImage);

var _achievement = require("../controllers/achievement");

var _achievement2 = _interopRequireDefault(_achievement);

var _socialFeed = require("../controllers/socialFeed");

var _socialFeed2 = _interopRequireDefault(_socialFeed);

var _newVoters = require("../controllers/newVoters");

var _newVoters2 = _interopRequireDefault(_newVoters);

var _config = require("../controllers/config");

var _config2 = _interopRequireDefault(_config);

var _enquiries = require("../controllers/enquiries");

var _enquiries2 = _interopRequireDefault(_enquiries);

var _multer = require("multer");

var _multer2 = _interopRequireDefault(_multer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// use this line for validation of params
// router.route('/register').post(validate(paramValidation.createUser), userCtrl.createUser);

router.get("/fetchUser", _user2.default.fetchUser);

router.get("/getProfile", _adminProfile2.default.getProfile);

router.get("/getPoll", _poll2.default.getPoll);

router.get("/fetchPoll", _poll2.default.fetchPoll);

router.post("/saveResponse", _poll2.default.saveResponse);

router.get("/getJourney", _journey2.default.getJourney);

router.get("/getVoters", _newVoters2.default.getVoters);

router.get("/getAchievement", _achievement2.default.getAchievement);

router.get("/getComplaints", _complaints2.default.getComplaints);

router.get("/getContact", _contact2.default.getContact);

router.get("/getsocialLink", _socialLinks2.default.getsocialLink);

router.get("/getsocialFeedsLink", _socialFeed2.default.getsocialFeedsLink);

router.get("/getEnquires", _enquiries2.default.getEnquires);

router.get("/fetchMeeting", _meeting2.default.fetchMeeting);

router.get("/getFeaturedImage", _featuredImage2.default.getFeaturedImage);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use(function (req, res, next) {
  _passport2.default.authenticate("jwt", _env2.default.passportOptions, function (error, userDtls, info) {
    //eslint-disable-line
    if (error) {
      var err = new _APIError2.default("token not matched", _httpStatus2.default.INTERNAL_SERVER_ERROR);
      return next(err);
    } else if (userDtls) {
      req.user = userDtls;
      next();
    } else {
      var _err = new _APIError2.default("token not matched " + info, _httpStatus2.default.UNAUTHORIZED);
      return next(_err);
    }
  })(req, res, next);
});

exports.default = router;
module.exports = exports["default"];
//# sourceMappingURL=user.js.map
