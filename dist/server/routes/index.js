'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

var _admin = require('./admin');

var _admin2 = _interopRequireDefault(_admin);

var _superAdmin = require('./superAdmin');

var _superAdmin2 = _interopRequireDefault(_superAdmin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import csvToJson from './csvToJson';


var router = _express2.default.Router();

/** GET /health-check - Check service health */
router.get('/health-check', function (req, res) {
  return res.send('OK');
});

router.get('/', function (req, res) {
  return res.send('It`s Working');
});

// mount user routes at /users
router.use('/users', _user2.default);

// mount auth routes at /auth
router.use('/auth', _auth2.default);

// mount admin routes at /admin
router.use('/admin', _admin2.default);

// mount super admin routes at /superadmin
router.use('/superadmin', _superAdmin2.default);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=index.js.map
