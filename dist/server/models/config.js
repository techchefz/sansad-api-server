"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var configSchema = new _mongoose2.default.Schema({
    adminConfig: { type: String, default: "admin", unique: true },
    gaId: { type: String, default: null },
    googleMapApi: { type: String, default: null },
    title: { type: String, default: null },
    email: { type: String, default: null },
    mobile: { type: String, default: null },
    address: { type: String, default: null },
    website: { type: String, default: null },
    facebookUrl: { type: String, default: null },
    twitterUrl: { type: String, default: null },
    youtubeUrl: { type: String, default: null },
    instagramUrl: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('configData', configSchema);
module.exports = exports["default"];
//# sourceMappingURL=config.js.map
