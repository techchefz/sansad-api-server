"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var SocialFeedsSchema = new _mongoose2.default.Schema({
  facebook: { type: String, default: null },
  consumer_key: { type: String, default: null },
  consumer_secret: { type: String, default: null },
  access_token_key: { type: String, default: null },
  access_token_secret: { type: String, default: null },
  youtube: { type: String, default: null },
  instagram: { type: String, default: null },
  userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model("SocialFeeds", SocialFeedsSchema);
module.exports = exports["default"];
//# sourceMappingURL=socialFeeds.js.map
