"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var UserSchema = new _mongoose2.default.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    date: { type: Date, default: Date.now() },
    userType: { type: String, default: 'user' },
    password: { type: String, default: null, unique: true },
    userId: { type: Schema.Types.ObjectId, ref: "AdminRegister", default: null }
});

exports.default = _mongoose2.default.model('User', UserSchema);
module.exports = exports["default"];
//# sourceMappingURL=user.js.map
