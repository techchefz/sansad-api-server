"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _ref;

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Schema = _mongoose2.default.Schema;

var UserSchema = new _mongoose2.default.Schema((_ref = {
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    userType: { type: String, default: 'admin' },
    password: { type: String, default: null, unique: true }
}, _defineProperty(_ref, "name", { type: String, default: null }), _defineProperty(_ref, "email", { type: String, default: null, unique: true }), _defineProperty(_ref, "phone", { type: String, default: null }), _defineProperty(_ref, "password", { type: String, default: null, unique: true }), _defineProperty(_ref, "userId", { type: Schema.Types.ObjectId, ref: "SuperAdmin", default: null }), _ref));

exports.default = _mongoose2.default.model('AdminRegister', UserSchema);
module.exports = exports["default"];
//# sourceMappingURL=adminRegister.js.map
