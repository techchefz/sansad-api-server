"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var profileSchema = new _mongoose2.default.Schema({
    name: { type: String, default: null },
    dateOfBirth: { type: String, default: null },
    qualification: { type: String, default: null },
    profilePicture: { type: String, default: null },
    about: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Profile', profileSchema);
module.exports = exports["default"];
//# sourceMappingURL=profile.js.map
