"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var PollSchema = new _mongoose2.default.Schema({
    question: { type: String, default: null },
    status: { type: Boolean, default: false },
    date: { type: Date, default: Date.now() },
    options: [{}],
    responseA: { type: Number, default: 1 },
    responseB: { type: Number, default: 1 },
    responseC: { type: Number, default: 1 },
    responseD: { type: Number, default: 1 },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Poll', PollSchema);
module.exports = exports["default"];
//# sourceMappingURL=poll.js.map
