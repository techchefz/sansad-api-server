"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var MeetingSchema = new _mongoose2.default.Schema({
    venueName: { type: String, default: null },
    venueDateTime: { type: Date, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Meeting', MeetingSchema);
module.exports = exports["default"];
//# sourceMappingURL=meeting.js.map
