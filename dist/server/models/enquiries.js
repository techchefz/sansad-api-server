"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var enquiresSchema = new _mongoose2.default.Schema({
    name: { type: String, default: null },
    mobile: { type: String, default: null },
    email: { type: String, default: null },
    date: { type: Date, default: Date.now() },
    query: { type: String, default: null },
    pageName: { type: String, default: null },
    pageUrl: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('enquires', enquiresSchema);
module.exports = exports["default"];
//# sourceMappingURL=enquiries.js.map
