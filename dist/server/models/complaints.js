"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var ComplaintsSchema = new _mongoose2.default.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null },
    date: { type: Date, default: Date.now() },
    mobileNo: { type: String, default: null },
    complaint: { type: String, default: null },
    status: { type: Boolean, default: false },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Complaints', ComplaintsSchema);
module.exports = exports["default"];
//# sourceMappingURL=complaints.js.map
