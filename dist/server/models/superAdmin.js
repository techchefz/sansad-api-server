'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var SuperAdminSchema = new _mongoose2.default.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    userType: { type: String, default: 'superAdmin' },
    password: { type: String, default: null, unique: true }
});

exports.default = _mongoose2.default.model('SuperAdmin', SuperAdminSchema);
module.exports = exports['default'];
//# sourceMappingURL=superAdmin.js.map
