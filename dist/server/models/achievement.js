"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var AchievementSchema = new _mongoose2.default.Schema({
  achievement: [{}],
  image: { type: String, default: null },
  userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model("Achievement", AchievementSchema);
module.exports = exports["default"];
//# sourceMappingURL=achievement.js.map
