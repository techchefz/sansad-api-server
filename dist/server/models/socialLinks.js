"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var SocialSchema = new _mongoose2.default.Schema({
    facebookLink: { type: String, default: null },
    twitterLink: { type: String, default: null },
    youtubeLink: { type: String, default: null },
    instagramLink: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Social', SocialSchema);
module.exports = exports["default"];
//# sourceMappingURL=socialLinks.js.map
