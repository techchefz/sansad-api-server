"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var ContactSchema = new _mongoose2.default.Schema({
    address: { type: String, default: null },
    email: { type: String, default: null },
    landlineNo: { type: String, default: null },
    faxNo: { type: String, default: null },
    latitude: { type: String, default: null },
    longitude: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

exports.default = _mongoose2.default.model('Contact', ContactSchema);
module.exports = exports["default"];
//# sourceMappingURL=contact.js.map
