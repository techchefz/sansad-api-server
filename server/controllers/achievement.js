import AchievementSchema from "../models/achievement";

function achievement(req, res) {
  let returnObj = {
    success: false,
    message: "No achievement found",
    data: {}
  };

  const achievement = JSON.parse(req.body.achievement);

  AchievementSchema.find({ userId: req.body.userId }).then(dataFound => {
    if (dataFound.length != 0) {
      AchievementSchema.findOneAndUpdate(
        { userId: req.body.userId },
        {
          $set: {
            achievement: achievement,
            image:
              req.file !== undefined
                ? "http://139.59.71.216:6060/uploads/" + req.file.filename
                : dataFound.image
          }
        },
        { new: true }
      ).then(achievementData => {
        if (achievementData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.message = "Achievement data updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      const newAchievement = new AchievementSchema({
        achievement: achievement,
        image: "http://139.59.71.216:6060/uploads/" + req.file.filename,
        userId: req.body.userId
      });
      newAchievement.save().then(achievementData => {
        returnObj.success = true;
        returnObj.message = "Achievement data added successfully";
        returnObj.data = achievementData;
        res.send(returnObj);
      });
    }
  });
}

function getAchievement(req, res) {
  let returnObj = {
    success: false,
    message: "No achievement found",
    data: {}
  };
  AchievementSchema.find({ userId: req.headers.userid })
    .populate("userId")
    .then(achievementData => {
      if (achievementData) {
        returnObj.success = true;
        returnObj.message = "achievement data found";
        returnObj.data = achievementData;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    });
}

export default {
  achievement,
  getAchievement
};
