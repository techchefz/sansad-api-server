import UserSchema from '../models/user'
import AdminRegisterSchema from "../models/adminRegister";
import SuperAdminSchema from "../models/superAdmin";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from '../../config/env';
const saltRounds = 10;

function superAdminLogin(req, res) {
    let returnObj = {
        success: false,
        message: "User login failed",
        jwtAccessToken: null,
        data: {}
    }
    let jwtAccessToken = null;
    SuperAdminSchema.findOne({ email: req.body.email }).then((foundUser) => {
        if (foundUser != null) {
            bcrypt.compare(req.body.password, foundUser.password, (err, result) => {
                if (result) {
                    jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
                    returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                    returnObj.success = true;
                    returnObj.message = "Authentication Successful"
                    delete foundUser.password;
                    returnObj.data = foundUser
                    returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                    res.send(returnObj)
                }
                else {
                    res.send(returnObj)
                }
            })
        }
    }).catch((error) => {
        returnObj.success = false;
        returnObj.message = error
        res.send(returnObj)
    })
}

function adminLogin(req, res) {
    let returnObj = {
        success: false,
        message: "User login failed",
        jwtAccessToken: null,
        data: {}
    }
    let jwtAccessToken = null;
    AdminRegisterSchema.findOne({ email: req.body.email }).then((foundUser) => {
        if (foundUser != null) {
            bcrypt.compare(req.body.password, foundUser.password, (err, result) => {
                if (result) {
                    jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
                    returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                    returnObj.success = true;
                    returnObj.message = "Authentication Successful"
                    delete foundUser.password;
                    returnObj.data = foundUser
                    returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                    res.send(returnObj)
                }
                else {
                    res.send(returnObj)
                }
            })
        }
    }).catch((error) => {
        returnObj.success = false;
        returnObj.message = error
        res.send(returnObj)
    })
}

function login(req, res) {
    let returnObj = {
        success: false,
        message: "User auth failed",
        jwtAccessToken: null,
        data: {}
    }
    let jwtAccessToken = null;
    let userId = req.body.userId;
    UserSchema.findOne({ email: req.body.email }).then((foundUser) => {
        if (foundUser != null) {
            bcrypt.compare(req.body.password, foundUser.password, (err, result) => {
                if (result) {
                    if (foundUser.userId == req.body.userId) {
                        jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
                        returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                        returnObj.success = true;
                        returnObj.message = "Authentication Successful"
                        delete foundUser.password;
                        returnObj.data = foundUser
                        returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
                        res.send(returnObj)
                    } else {
                        res.send(returnObj)
                    }
                }
                else {
                    res.send(returnObj)
                }
            })
        } else {
            res.send(returnObj)
        }
    }).catch((error) => {
        returnObj.success = false;
        returnObj.message = error
        res.send(returnObj)
    })
}

function changePassword(req, res) {
    let returnObj = {
        success: false,
        message: "password not changed",
        data: {}
    }
    let HashPassword = null;
    if (req.body.password != null) {
        bcrypt.hash(req.body.password, saltRounds)
            .then((hashPassword) => {
                UserSchema.findOneAndUpdate({ email: req.body.email },
                    { $set: { password: hashPassword } }, { new: true })
                    .then((passwordChanged) => {
                        if (passwordChanged == null) {
                            res.send(returnObj)
                        } else {
                            returnObj.success = true;
                            returnObj.message = "password changed successfully";
                            res.send(returnObj)
                        }
                    })
            })
    }



}

export default {
    login,
    changePassword,
    adminLogin,
    superAdminLogin
}