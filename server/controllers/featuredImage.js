import FeaturedImageSchema from "../models/featuredImage";

function featuredImage(req, res) {
    let returnObj = {
        success: false,
        message: "No featured Image found",
        data: []
    }
    let gallery = [];
    req.files.forEach(pics => {
        gallery.push("http://139.59.71.216:6060/uploads/" + pics.filename)
    });
    FeaturedImageSchema.find({ userId: req.body.userId })
        .then((dataFound) => {
            if (dataFound.length != 0) {
                FeaturedImageSchema.findOneAndUpdate({ userId: req.body.userId },
                    { $set: { "image": gallery } }, { new: true })
                    .then((featuredImageData) => {
                        if (featuredImageData == null) {
                            res.send(returnObj)
                        } else {
                            returnObj.success = true;
                            returnObj.message = "featuredImage Data updated successfully";
                            returnObj.data = featuredImageData;
                            res.send(returnObj)
                        }
                    })
            } else {
                const featuredImage = new FeaturedImageSchema({
                    userId: req.body.userId,
                    image: gallery
                });
                featuredImage.save().then((featuredImageData) => {
                    returnObj.success = true;
                    returnObj.message = "featured Image data added successfully"
                    returnObj.data = featuredImageData;
                    res.send(returnObj)
                })
            }
        })
}

function getFeaturedImage(req, res) {
    let returnObj = {
        success: false,
        message: "No featured Image found",
        data: []
    }
    let featuredImage = [];
    FeaturedImageSchema.findOne({ userId: req.headers.userid })
        .then((dataFound) => {
            featuredImage = dataFound.image;
            returnObj.success = true;
            returnObj.message = "Featured Image Found";
            returnObj.data = featuredImage;
            res.send(returnObj)
        })
}

export default {
    featuredImage,
    getFeaturedImage
}
