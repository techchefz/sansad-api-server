// var Twit = require('twit')
import socialFeedsSchema from "../models/socialFeeds";
var Twit = require("twitter");
var params = { screen_name: 'a1331g' }

function twitterFeeds(req, res) {
	let returnObj = {
		success: false,
		message: "data fetch failed",
		data: {}
	}
	socialFeedsSchema.find({ userId: req.body.userId }).then((dataFound) => {
		if (dataFound[0].consumer_key == "" || dataFound[0].consumer_secret == "" || dataFound[0].access_token_key == "" || dataFound[0].access_token_secret == "") {
			console.log('===============IF=====================');
		} else {
			var client = new Twit({
				consumer_key: dataFound[0].consumer_key,
				consumer_secret: dataFound[0].consumer_secret,
				access_token_key: dataFound[0].access_token_key,
				access_token_secret: dataFound[0].access_token_secret
			});


			client.get('statuses/user_timeline', params, function (err, data, response) {
				if (err) {
					console.log(err)
				}
				let Data = null;
				Data = JSON.parse(response.body);
				let tweets = [];
				Data.forEach(foundData => {
					if (tweets.length < 10) {
						tweets.push(foundData)
					}
				});
				returnObj.data = (tweets);
				res.send(returnObj)
				// console.log(JSON.stringify(response))

			})
		}
	})
}

function youtubeId(req, res) {
	let returnObj = {
		success: false,
		message: "data fetch failed",
		youtubeId: {}
	}
	socialFeedsSchema.find({ userId: req.body.userId }).then((dataFound) => {
		returnObj.youtubeId = dataFound[0].youtube
		returnObj.message = "Id found"
		res.send(returnObj)
	})
}

export default {
	twitterFeeds,
	youtubeId
}