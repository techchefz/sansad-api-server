import ContactSchema from "../models/contact";

function contact(req, res) {
    let returnObj = {
        success: false,
        message: "No contact found",
        data: {}
    }
    ContactSchema.find({ userId: req.body.userId }).populate("userId")
    .then((dataFound) => {
        if (dataFound.length != 0) {
            ContactSchema.findOneAndUpdate({ userId: req.body.userId },
                {
                    $set: {
                        "address": req.body.address,
                        "email": req.body.email,
                        "landlineNo": req.body.landlineNo,
                        "faxNo": req.body.faxNo,
                        "latitude": req.body.latitude,
                        "longitude": req.body.longitude,
                    }
                }, { new: true })
                .then((contactData) => {
                    if (contactData == null) {
                        res.send(returnObj)
                    } else {
                        returnObj.success = true;
                        returnObj.data = contactData;
                        returnObj.message = "contact details updated successfully";
                        res.send(returnObj)
                    }
                })
        } else {
            const newContact = new ContactSchema({
                address: req.body.address,
                email: req.body.email,
                landlineNo: req.body.landlineNo,
                faxNo: req.body.faxNo,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                userId: req.body.userId
            });
            newContact.save().then((contactData) => {
                returnObj.success = true;
                returnObj.message = "contact details added successfully"
                returnObj.data = contactData;
                res.send(returnObj)
            })
        }
    })
}

function getContact(req, res) {
    let returnObj = {
        success: false,
        message: "No data found",
        data: {}
    }
    ContactSchema.find({ userId: req.headers.userid })
        .then((contactData) => {
            if (contactData) {
                returnObj.success = true;
                returnObj.message = "data found"
                returnObj.data = contactData;
                res.send(returnObj)
            } else {
                res.send(returnObj)
            }
        })
}

export default {
    contact,
    getContact
}