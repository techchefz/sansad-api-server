import PollSchema from "../models/poll";

function poll(req, res) {
    let returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    }
    const newPoll = new PollSchema({
        question: req.body.question,
        options: req.body.options,
        userId: req.body.userId
    });
    newPoll.save().then((pollData) => {
        returnObj.success = true;
        returnObj.message = "New poll saved"
        returnObj.data = pollData;
        res.send(returnObj)
    }).catch((error) => {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function getPoll(req, res) {
    let returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    }
    console.log('====================================');
    console.log(req.headers);
    console.log('====================================');
    PollSchema.find({ userId: req.headers.userid }).populate("userId")
        .then((foundPolls) => {
            if (foundPolls) {
                returnObj.success = true;
                returnObj.message = "poll found"
                returnObj.data = foundPolls;
                res.send(returnObj)
            } else {
                res.send(returnObj)
            }
        })
}

function fetchPoll(req, res) {
    let returnObj = {
        success: false,
        message: "No poll found",
        data: {}
    }
    PollSchema.find({ userId: req.headers.userid }).populate("userId")
        .then((foundPolls) => {
            if (foundPolls) {
                foundPolls.forEach((data)=>{
                    if(data.status == true){
                        returnObj.success = true;
                        returnObj.message = "poll found"
                        returnObj.data = data;
                        res.send(returnObj)
                    } else {
                        // res.send(returnObj)
                    }
                })
            } else {
                res.send(returnObj)
            }
        })
}

function pollStatus(req, res) {
    let returnObj = {
        success: false,
        message: "status updation failed",
        data: {}
    }
    var userID = req.body.userId;
    PollSchema.update({ userId: req.body.userId }, { $set: { status: false, } }, { multi: true })
        .then((data) => {
            console.log('====================================');
            console.log(data);
            console.log('====================================');
            PollSchema.findOneAndUpdate({ _id: req.body._id },
                {
                    $set: {
                        status: req.body.status,
                    }
                }, { new: true })
                .then((foundPolls) => {
                    returnObj.success = true;
                    returnObj.message = "status updated successfully"
                    returnObj.data = foundPolls;
                    res.send(returnObj)
                })
        })
}

function saveResponse(req, res) {
    let returnObj = {
        success: false,
        message: "Response Updation Failed",
        data: {}
    }
    let optionA = null;
    let optionB = null;
    let optionC = null;
    let optionD = null;
    let users = [];
    console.log('====================================')
    console.log(req.body)
    console.log('====================================')
    PollSchema.find({ _id: req.body._id })
        .then(pollFound => {
            console.log('====================================')
            console.log(req.body)
            console.log('====================================')
            pollFound.forEach(data => {
                if (req.body.response == "a") {
                    optionA = data.responseA
                    PollSchema.findOneAndUpdate({ _id: req.body._id },
                        { $set: { responseA: optionA + 1 } }, { new: true })
                    optionA = data.responseA
                    PollSchema.findOneAndUpdate({ _id: req.body._id },
                        { $set: { responseA: optionA + 1 } }, { new: true })
                        .then(foundPoll => {
                            returnObj.message = "poll found"
                            returnObj.data = foundPoll
                            res.send(returnObj)
                        })
                } else if (req.body.response == "b") {
                    optionB = data.responseB
                    PollSchema.findOneAndUpdate({ _id: req.body._id },
                        { $set: { responseB: optionB + 1 } }, { new: true })
                        .then(foundPoll => {
                            returnObj.message = "poll found"
                            returnObj.data = foundPoll
                            res.send(returnObj)
                        })
                } else if (req.body.response == "c") {
                    optionC = data.responseC
                    PollSchema.findOneAndUpdate({ _id: req.body._id },
                        { $set: { responseC: optionC + 1 } }, { new: true })
                        .then(foundPoll => {
                            returnObj.message = "poll found"
                            returnObj.data = foundPoll
                            res.send(returnObj)
                        })
                } else if (req.body.response == "d") {
                    optionD = data.responseD
                    PollSchema.findOneAndUpdate({ _id: req.body._id },
                        { $set: { responseD: optionD + 1 } }, { new: true })
                        .then(foundPoll => {
                            returnObj.message = "poll found"
                            returnObj.data = foundPoll
                            res.send(returnObj)
                        })
                }
            })
        })
}
export default {
    poll,
    getPoll,
    fetchPoll,
    saveResponse,
    pollStatus
}