import VotersSchema from "../models/newVoters";
import moment from "moment";

function newVoters(req, res) {
	let returnObj = {
		success: false,
		message: "No voter found",
		data: {}
	}
	const newVoter = new VotersSchema({
		name: req.body.name,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		userId: req.body.userId
	});
	newVoter.save().then((voters) => {
		returnObj.success = true;
		returnObj.message = "New voter saved"
		returnObj.data = voters;
		res.send(returnObj)
	}).catch((error) => {
		console.log('==============error======================');
		console.log(error);
		console.log('==============error======================');
	});
}

// function dashboardComplaintData(req, res) {
//     console.log('====================================');
//     console.log("chala");
//     console.log('====================================');
//     let returnObj = {
//         success: false,
//         message: "No data found",
//         data: {}
//     }
//     let todayDate = Date();
//     let todayComplaints = 0;
//     let totalComplaint = 0;
//     let solvedComplaint = 0;
//     let unsolvedComplaint = 0;
//     VotersSchema.find({ userId: req.headers.userid }).populate("userId")
//         .then(dataFound => {
//             console.log('====================================');
//             console.log(dataFound);
//             console.log('====================================');
//             if (dataFound) {
//                 // let tComplaint = dataFound.length;
//                 totalComplaint = dataFound.length;
//                 dataFound.forEach((dataFound) => {
//                     if (moment(Date()).format("DD-MM-YYYY") == moment(dataFound.date).format("DD-MM-YYYY")) {
//                         let tdayComplaint = []
//                         tdayComplaint.push(dataFound);
//                         todayComplaints = tdayComplaint.length;
//                     }
//                     if (dataFound.status == true) {
//                         solvedComplaint = solvedComplaint + 1;
//                     }
//                     if (dataFound.status == false) {
//                         console.log('===============dataFound.status=====================');
//                         console.log(dataFound.status);
//                         console.log('====================================');
//                         solvedComplaint = solvedComplaint + 1;
//                     }
//                 })
//             } else {
//                 res.send(returnObj)
//             }
//         }).then(() => {
//             returnObj.success = true;
//             returnObj.message = "Data found"
//             returnObj.data = {
//                 todayComplaints,
//                 totalComplaint,
//                 solvedComplaint,
//                 unsolvedComplaint
//             }
//             res.send(returnObj)
//         })
// }

function getVoters(req, res) {
	let returnObj = {
		success: false,
		message: "No voter found",
		data: {}
	}
	VotersSchema.find({ userId: req.headers.userid }).populate("userId")
		.then((foundData) => {
			if (foundData) {
				returnObj.success = true;
				returnObj.message = "data found"
				returnObj.data = foundData;
				res.send(returnObj)
			} else {
				res.send(returnObj)
			}
		})
}

export default {
	newVoters,
	getVoters,
	// dashboardComplaintData
}