import adminRegisterSchema from "../models/adminRegister";
import bcrypt from "bcrypt";
const saltRounds = 10;

function adminRegister(req, res) {
    let returnObj = {
        success: false,
        message: "User Already Exist`s",
        data: {}
    }
    adminRegisterSchema.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser != null) {
                res.send(returnObj)
            } else {
    bcrypt.hash(req.body.password, saltRounds)
                    .then((hashPassword) => {
                        const newUser = new adminRegisterSchema({
                            name: req.body.name,
                            phone: req.body.mobileNo,
                            email: req.body.email,
                            password: hashPassword,
                        });
                        newUser.save().then((savedUser) => {
                            returnObj.success = true;
                            returnObj.message = "Users Registered Successfully!"
                            returnObj.data = savedUser;
                            res.send(returnObj)
                        });
                    });
            }
        }).catch((error) => {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        })
}

function fetchAdmin(req, res){
    let returnObj = {
        success: false,
        message: "No user found",
        data: []
    }
    adminRegisterSchema.find().then((userFound)=>{
        returnObj.success = true;
        returnObj.message = "Users found"
        returnObj.data = userFound;
        res.send(returnObj)
    })
}


export default {
    adminRegister,
    fetchAdmin
}
