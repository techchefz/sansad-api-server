import JourneySchema from "../models/journey";

function journey(req, res) {
    let returnObj = {
        success: false,
        message: "No journey found",
        data: {}
    }
    JourneySchema.find({ userId: req.body.userId }).then((dataFound) => {
        if (dataFound.length != 0) {
            JourneySchema.findOneAndUpdate({ userId: req.body.userId },
                {
                    $set: {
                        "journey": req.body.journey,
                        "image": req.file !== undefined ? "http://139.59.71.216:6060/uploads/" + req.file.filename : dataFound.image
                    }
                }, { new: true })
                .then((journeyData) => {
                    if (journeyData == null) {
                        res.send(returnObj)
                    } else {
                        returnObj.success = true;
                        returnObj.message = "journey data updated successfully";
                        res.send(returnObj)
                    }
                })
        } else {
            const newJourney = new JourneySchema({
                journey: req.body.journey,
                image: "http://139.59.71.216:6060/uploads/" + req.file.filename,
                userId: req.body.userId
            });
            newJourney.save().then((journeyData) => {
                returnObj.success = true;
                returnObj.message = "journey data added successfully"
                returnObj.data = journeyData;
                res.send(returnObj)
            })
        }
    })
}

function getJourney(req, res) {
    let returnObj = {
        success: false,
        message: "No journey found",
        data: {}
    }
    JourneySchema.find({ userId: req.headers.userid }).populate("userId")
        .then((journeyData) => {
            if (journeyData) {
                returnObj.success = true;
                returnObj.message = "journey data found"
                returnObj.data = journeyData;
                res.send(returnObj)
            } else {
                res.send(returnObj)
            }
        })
}

export default {
    journey,
    getJourney
}
