import enquiresSchema from "../models/enquiries";

function enquires(req, res) {
    let returnObj = {
        success: false,
        message: "No enquiry found",
        data: {}
    }
    const newEnquiry = new enquiresSchema({
        name: req.body.name,
        mobile: req.body.mobile,
        email: req.body.email,
        query: req.body.query,
        pageName: req.body.pageName,
        pageURL: req.body.pageURL,
    });
    newEnquiry.save().then((enquiryData) => {
        returnObj.success = true;
        returnObj.message = "New query saved"
        returnObj.data = enquiryData;
        res.send(returnObj)
    }).catch((error) => {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    })
}

function getEnquires(req, res) {
        let returnObj = {
        success: false,
        message: "No Enquery Found",
        data: []
    }

    let enquery = [];
    enquiresSchema.find().then((foundEnquery) => {
        enquery = foundEnquery;
        returnObj.success = true;
        returnObj.message = "Enquery Found";
        returnObj.data = enquery;
        res.send(returnObj)
    }).catch((error) => {
        returnObj.success = false;
        returnObj.message = "No Enquery Found";
        res.send(returnObj)
    })
}

export default {
    enquires,
    getEnquires
}