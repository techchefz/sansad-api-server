import SocialSchema from "../models/socialLinks";

function socialLink(req, res) {
    let returnObj = {
        success: false,
        message: "No social Link found",
        data: {}
    }
    SocialSchema.find({ userId: req.body.userId }).then((dataFound) => {
        if (dataFound.length > 0) {
            SocialSchema.findOneAndUpdate({ userId: req.body.userId },
                {
                    $set: {
                        "facebookLink": req.body.facebookLink,
                        "twitterLink": req.body.twitterLink,
                        "youtubeLink": req.body.youtubeLink,
                        "instagramLink": req.body.instagramLink,
                        "userId": req.body.userId
                    }
                }, { new: true })
                .then((socialData) => {
                    if (socialData == null) {
                        res.send(returnObj)
                    } else {
                        returnObj.success = true;
                        returnObj.data = socialData;
                        returnObj.message = "social details updated successfully";
                        res.send(returnObj)
                    }
                })
        } else {
            const newSocialLink = new SocialSchema({
                facebookLink: req.body.facebookLink,
                twitterLink: req.body.twitterLink,
                youtubeLink: req.body.youtubeLink,
                instagramLink: req.body.instagramLink,
                userId: req.body.userId
            });
            newSocialLink.save().then((socialData) => {
                returnObj.success = true;
                returnObj.message = "social details added successfully"
                returnObj.data = socialData;
                res.send(returnObj)
            })
        }
    })
}

function getsocialLink(req, res) {
    let returnObj = {
        success: false,
        message: "No data found",
        data: {}
    }
    SocialSchema.find({ userId: req.headers.userid }).populate("userId")
        .then((socialData) => {
            if (socialData) {
                returnObj.success = true;
                returnObj.message = "data found"
                returnObj.data = socialData;
                res.send(returnObj)
            } else {
                res.send(returnObj)
            }
        })
}

export default {
    socialLink,
    getsocialLink
}