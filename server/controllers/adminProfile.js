import profileSchema from "../models/profile";
import userSchema from "../models/user";

function profile(req, res) {
    let returnObj = {
        success: false,
        message: "No profile found",
        data: {}
    }
    profileSchema.findOne({ userId: req.body.userId }).populate("userId").then((dataFound) => {
        if (dataFound) {
            profileSchema.findOneAndUpdate({ userId: req.body.userId },
                {
                    $set: {
                        "name": req.body.name,
                        "dateOfBirth": req.body.dateOfBirth,
                        "qualification": req.body.qualification,
                        "profilePicture":req.files !== undefined ? "http://139.59.71.216:6060/uploads/" + req.file.filename : dataFound.profilePicture ,
                        "about": req.body.about
                    }
                }, { new: true })
                .then((profileData) => {
                    if (profileData == null) {
                        res.send(returnObj)
                    } else {
                        returnObj.success = true;
                        returnObj.message = "journey data updated successfully";
                        returnObj.data = profileData;
                        res.send(returnObj)
                    }
                })
        } else {
            const newProfile = new profileSchema({
                name: req.body.name,
                dateOfBirth: req.body.dateOfBirth,
                qualification: req.body.qualification,
                profilePicture: "http://139.59.71.216:6060/uploads/" + req.file.filename,
                about: req.body.about,
                userId: req.body.userId
            });
            newProfile.save().then((profileData) => {
                returnObj.success = true;
                returnObj.message = "New profile saved"
                returnObj.data = profileData;
                res.send(returnObj)
            }).catch((error) => {
                console.log(error);
            })
        }
    })
}

function getProfile(req, res) {
    let returnObj = {
        success: false,
        message: "No Profile Found",
        data: null
    }
    let profile = [];
    profileSchema.find({ userId: req.headers.userid }).populate("userId").then((foundProfile) => {
            if (foundProfile) {
                profile = foundProfile;
                returnObj.success = true;
                returnObj.message = "Profile Found";
                returnObj.data = profile;
                res.send(returnObj)
            } else {
                res.send(returnObj)
            }
    }).catch((error) => {
        console.log(error);        
    })
}

export default {
    profile,
    getProfile
}
