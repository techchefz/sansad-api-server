import SocialFeedsSchema from "../models/socialFeeds";

function socialFeedsLink(req, res) {
  let returnObj = {
    success: false,
    message: "No social Link found",
    data: {}
  };
  SocialFeedsSchema.find({ userId: req.body.userId }).then(dataFound => {
    if (dataFound.length > 0) {
      SocialFeedsSchema.findOneAndUpdate(
        { userId: req.body.userId },
        {
          $set: {
            facebook: req.body.facebook,
            consumer_key: req.body.consumer_key,
            consumer_secret: req.body.consumer_secret,
            access_token_key: req.body.access_token_key,
            access_token_secret: req.body.access_token_secret,
            youtube: req.body.youtube,
            instagram: req.body.instagram,
            userId: req.body.userId
          }
        },
        { new: true }
      ).then(socialData => {
        if (socialData == null) {
          res.send(returnObj);
        } else {
          returnObj.success = true;
          returnObj.data = socialData;
          returnObj.message = "social details updated successfully";
          res.send(returnObj);
        }
      });
    } else {
      const newSocial = new SocialFeedsSchema({
        facebook: req.body.facebook,
        consumer_key: req.body.consumer_key,
        consumer_secret: req.body.consumer_secret,
        access_token_key: req.body.access_token_key,
        access_token_secret: req.body.access_token_secret,
        youtube: req.body.youtube,
        instagram: req.body.instagram,
        userId: req.body.userId
      });
      newSocial.save().then(socialData => {
        returnObj.success = true;
        returnObj.message = "social details added successfully";
        returnObj.data = socialData;
        res.send(returnObj);
      });
    }
  });
}

function getsocialFeedsLink(req, res) {
  let returnObj = {
    success: false,
    message: "No data found",
    data: {}
  };
  SocialFeedsSchema.findOne({ userId: req.headers.userid })
    .populate("userId")
    .then(socialData => {
      if (socialData) {
        returnObj.success = true;
        returnObj.message = "data found";
        returnObj.data = socialData;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    });
}

export default {
  socialFeedsLink,
  getsocialFeedsLink
};
