import UserSchema from "../models/user";
import bcrypt from "bcrypt";
import moment from "moment";

const saltRounds = 10;


function createUser(req, res) {
    let returnObj = {
        success: false,
        message: "User Already Exist`s",
        data: {}
    }
            console.log('====================================');
            console.log(req.body);
            console.log('====================================');
    UserSchema.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser != null) {
                res.send(returnObj)
            } else {
    bcrypt.hash(req.body.password, saltRounds)
                    .then((hashPassword) => {
                        const newUser = new UserSchema({
                            name: req.body.name,
                            phone: req.body.mobileNo,
                            email: req.body.email,
                            password: hashPassword,
                            userType: req.body.userType,
                            userId: req.body.userId
                        });
                        newUser.save().then((savedUser) => {
                            returnObj.success = true;
                            returnObj.message = "Users Registered Successfully!"
                            returnObj.data = savedUser;
                            res.send(returnObj)
                        });
                    });
            }
        }).catch((error) => {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        })
}

function fetchUser(req, res){
    let returnObj = {
        success: false,
        message: "No user found",
        data: []
    }
    UserSchema.find({ userId: req.headers.userid }).then((userFound)=>{
        if (userFound == null) {
            res.send(returnObj)
        } else {
            returnObj.success = true;
            returnObj.message = "Users found"
            returnObj.data = userFound;
            res.send(returnObj)
        }
    })
}

function fetchAllUsers(req, res){
    let returnObj = {
        success: false,
        message: "No user found",
        data: []
    }
    UserSchema.find().then((userFound)=>{
        if (userFound == null) {
            res.send(returnObj)
        } else {
            returnObj.success = true;
            returnObj.message = "Users found"
            returnObj.data = userFound;
            res.send(returnObj)
        }
    })
}

function review(req, res) {
    let returnObj = {
        success: false,
        message: "User review error",
        data: {}
    }
    
    const newReview = new ReviewSchema({
        name: req.body.name,
        page: req.body.page,
        pageUrl: req.body.pageUrl,
        email: req.body.email,
        subject: req.body.subject,
        rating: req.body.rating,
        message: req.body.message,
    });
    newReview.save().then((userReview) => {
        returnObj.success = true;
        returnObj.message = "User review saved"
        returnObj.data = userReview;
        res.send(returnObj)
    }).catch((error) => {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    })

}

function getAllReviews(req, res) {

    let returnObj = {
        success: false,
        message: "No Review Found",
        data: []
    }

    let reviews = [];
    ReviewSchema.find().then((foundItems) => {
        reviews = foundItems;
        returnObj.success = true;
        returnObj.message = "Review Found";
        returnObj.data = reviews;
        res.send(returnObj)
    }).catch((error) => {
        returnObj.success = false;
        returnObj.message = "Reviews not Found";
        res.send(returnObj)
    })

}

function reviewStatusUpdate(req, res) {
    let returnObj = {
        success: false,
        message: "Status not updated",
        data: {}
    }
    ReviewSchema.findOneAndUpdate({ _id: req.body._id },
        { $set: { "status": req.body.status } }, { new: true })
        .then((statusUpdate) => {
            if (statusUpdate == null) {
                res.send(returnObj)
            } else {
                returnObj.success = true;
                returnObj.message = "status updated successfully";
                res.send(returnObj)
            }
        })
}

function reviewDelete(req, res) {
    let returnObj = {
        success: false,
        message: "No review found",
        data: {}
    }
    ReviewSchema.findOneAndRemove({ _id: req.body._id })
        .then((reviewRemoved) => {
            if (reviewRemoved == null) {
                res.send(returnObj)
            } else {
                returnObj.success = true;
                returnObj.message = "Review deleted successfully";
                res.send(returnObj)
            }
        })
}


function Itinerary(req, res) {
    let returnObj = {
        success: false,
        message: "No data added",
        data: {}
    }
    const newItinerary = new ItinerarySchema({
        pageTitle: req.body.pageTitle,
        metaTitle: req.body.metaTitle,
        pageUrl: req.body.pageUrl,
        metaDescription: req.body.metaDescription,
        metaKeywords: req.body.metaKeywords,
        description: req.body.description,
        imagePath: "http://localhost:3000/uploads/" + req.file.filename,
        itineraryDays: req.body.itineraryDays,
        tripDetail: req.body.tripDetail,
    });
    newItinerary.save().then((ItineraryData) => {
        returnObj.success = true;
        returnObj.message = "Data added"
        returnObj.data = ItineraryData;
        res.send(returnObj)
    }).catch((error) => {
        console.log('==============error======================');
        console.log(error);
        console.log('==============error======================');
    });
}

function fetchUserMonthlyCount(req, res){
    let returnObj = {
        success: false,
        message: "No Data found",
        data: []
      };
      let Data = null;
      let Jan = 0;
      let Feb = 0;
      let Mar = 0;
      let Apr = 0;
      let May = 0;
      let Jun = 0;
      let Jul = 0;
      let Aug = 0;
      let Sept = 0;
      let Oct = 0;
      let Nov = 0;
      let Dec = 0;
      let currentYear = moment(Date()).format("YY");
      UserSchema.find()
        .then(data => {
          data.forEach(allData => {
            if (currentYear == moment(allData.date).format("YY")) {
              if (moment(allData.date).format("MMM") == moment("1").format("MMM")) {
                Jan = Jan + 1;
              } else if (moment(allData.date).format("MMM") == moment("2").format("MMM")) {
                Feb = Feb + 1;
              } else if (moment(allData.date).format("MMM") == moment("3").format("MMM")) {
                Mar = Mar + 1;
              } else if (moment(allData.date).format("MMM") == moment("4").format("MMM")) {
                Apr = Apr + 1;
              } else if (moment(allData.date).format("MMM") == moment("5").format("MMM")) {
                May = May + 1;
              } else if (moment(allData.date).format("MMM") == moment("6").format("MMM")) {
                Jun = Jun + 1;
              } else if (moment(allData.date).format("MMM") == moment("7").format("MMM")) {
                Jul = Jul + 1;
              } else if (moment(allData.date).format("MMM") == moment("8").format("MMM")) {
                Aug = Aug + 1;
              } else if (moment(allData.date).format("MMM") == moment("9").format("MMM")) {
                Sept = Sept + 1;
              } else if (moment(allData.date).format("MMM") == moment("10").format("MMM")) {
                Oct = Oct + 1;
              } else if (moment(allData.date).format("MMM") == moment("11").format("MMM")) {
                Nov = Nov + 1;
              } else if (moment(allData.date).format("MMM") == moment("12").format("MMM")) {
                Dec = Dec + 1;
              }
            }
          });
          Data = {
            Jan: Jan,
            Feb: Feb,
            Mar: Mar,
            Apr: Apr,
            May: May,
            Jun: Jun,
            Jul: Jul,
            Aug: Aug,
            Sept: Sept,
            Oct: Oct,
            Nov: Nov,
            Dec: Dec
          };
        })
        .then(() => {
          returnObj.success = true;
          returnObj.message = "Monthly Reviews count";
          returnObj.data = Data;
          res.send(returnObj);
        });
}

export default {
    createUser,
    fetchUser,
    fetchAllUsers,
    review,
    Itinerary,
    reviewStatusUpdate,
    getAllReviews,
    reviewDelete,
    fetchUserMonthlyCount
}
