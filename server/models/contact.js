import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ContactSchema = new mongoose.Schema({
    address: { type: String, default: null },
    email: { type: String, default: null },
    landlineNo: { type: String, default: null },
    faxNo: { type: String, default: null },
    latitude: { type: String, default: null },
    longitude: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Contact', ContactSchema);