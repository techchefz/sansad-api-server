import mongoose from "mongoose";

var Schema = mongoose.Schema;

const SocialSchema = new mongoose.Schema({
    facebookLink: { type: String, default: null },
    twitterLink: { type: String, default: null },
    youtubeLink: { type: String, default: null },
    instagramLink: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Social', SocialSchema);