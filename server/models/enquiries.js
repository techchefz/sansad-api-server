import mongoose from "mongoose";

var Schema = mongoose.Schema;

const enquiresSchema = new mongoose.Schema({
    name: { type: String, default: null},
    mobile: { type: String, default: null, },
    email: { type: String, default: null, },
    date: { type: Date, default: Date.now() },
    query: { type: String, default: null, },
    pageName: { type: String, default: null, },
    pageUrl: { type: String, default: null, },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('enquires', enquiresSchema);