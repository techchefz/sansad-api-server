import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ReviewsSchema = new mongoose.Schema({
    name: { type: String, default: null, },
    date: { type: Date, default: Date.now() },
    page: { type: String, default: null, },
    pageUrl: { type: String, default: null, },
    email: { type: String, default: null, },
    subject: { type: String, default: null, },
    rating: { type: String, default: null },
    message: { type: String, default: null, },    
    status: { type: Boolean, default: false, },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Reviews', ReviewsSchema);