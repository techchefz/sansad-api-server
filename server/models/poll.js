import mongoose from "mongoose";

var Schema = mongoose.Schema;

const PollSchema = new mongoose.Schema({
    question: { type: String, default: null },
    status:{ type: Boolean, default: false },
    date: { type: Date, default: Date.now() },
    options: [{}],
    responseA: { type: Number, default: 1 },
    responseB: { type: Number, default: 1 },
    responseC: { type: Number, default: 1 },
    responseD: { type: Number, default: 1 },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Poll', PollSchema);