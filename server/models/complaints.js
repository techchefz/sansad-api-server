import mongoose from "mongoose";

var Schema = mongoose.Schema;

const ComplaintsSchema = new mongoose.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null },
    date: { type: Date, default: Date.now() },
    mobileNo: { type: String, default: null },
    complaint: { type: String, default: null },
    status: { type: Boolean, default: false },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Complaints', ComplaintsSchema);