import mongoose from "mongoose";

var Schema = mongoose.Schema;

const FeaturedImageSchema = new mongoose.Schema({
    image: [{ type: String, default: null }],
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('FeaturedImage', FeaturedImageSchema);