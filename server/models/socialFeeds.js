import mongoose from "mongoose";

var Schema = mongoose.Schema;

const SocialFeedsSchema = new mongoose.Schema({
  facebook: { type: String, default: null },
  consumer_key: { type: String, default: null },
  consumer_secret: { type: String, default: null },
  access_token_key: { type: String, default: null },
  access_token_secret: { type: String, default: null },
  youtube: { type: String, default: null },
  instagram: { type: String, default: null },
  userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

export default mongoose.model("SocialFeeds", SocialFeedsSchema);
