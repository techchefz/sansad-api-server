import mongoose from "mongoose";

var Schema = mongoose.Schema;

const AchievementSchema = new mongoose.Schema({
  achievement: [{}],
  image: { type: String, default: null },
  userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
});

export default mongoose.model("Achievement", AchievementSchema);
