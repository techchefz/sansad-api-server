import mongoose from "mongoose";

var Schema = mongoose.Schema;

const VotersSchema = new mongoose.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null },
    date: { type: Date, default: Date.now() },
    mobileNo: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, ref: "User", default: null }
})

export default mongoose.model('Voters', VotersSchema);