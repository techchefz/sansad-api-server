import mongoose from "mongoose";

var Schema = mongoose.Schema;

const SuperAdminSchema = new mongoose.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    userType: { type: String, default: 'superAdmin' },
    password: { type: String, default: null, unique: true, },
})

export default mongoose.model('SuperAdmin', SuperAdminSchema);