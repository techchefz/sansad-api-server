import mongoose from "mongoose";

var Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    userType: { type: String, default: 'admin' },
    password: { type: String, default: null, unique: true, },
    name: { type: String, default: null },
    email: { type: String, default: null, unique: true },
    phone: { type: String, default: null },
    password: { type: String, default: null, unique: true, },
    userId: { type: Schema.Types.ObjectId, ref: "SuperAdmin", default: null }
})

export default mongoose.model('AdminRegister', UserSchema);