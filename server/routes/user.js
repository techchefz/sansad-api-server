import express from "express";
import validate from "express-validation";
import httpStatus from "http-status";
import passport from "passport";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import paramValidation from "../../config/param-validation";
import userCtrl from "../controllers/user";
import profileCtrl from "../controllers/adminProfile";
import pollCtrl from "../controllers/poll";
import journeyCtrl from "../controllers/journey";
import complaintCtrl from "../controllers/complaints";
import contactCtrl from "../controllers/contact";
import socialCtrl from "../controllers/socialLinks";
import meetingCtrl from "../controllers/meeting";
import featuredImageCtrl from "../controllers/featuredImage";
import achievementCtrl from "../controllers/achievement";
import socialFeedCtrl from "../controllers/socialFeed";
import votersCtrl from "../controllers/newVoters";
import configCtrl from "../controllers/config";
import queryCtrl from "../controllers/enquiries";

import multer from "multer";

const router = express.Router();

// use this line for validation of params
// router.route('/register').post(validate(paramValidation.createUser), userCtrl.createUser);

router.get("/fetchUser", userCtrl.fetchUser);

router.get("/getProfile", profileCtrl.getProfile);

router.get("/getPoll", pollCtrl.getPoll);

router.get("/fetchPoll", pollCtrl.fetchPoll);

router.post("/saveResponse", pollCtrl.saveResponse);

router.get("/getJourney", journeyCtrl.getJourney);

router.get("/getVoters", votersCtrl.getVoters);

router.get("/getAchievement", achievementCtrl.getAchievement);

router.get("/getComplaints", complaintCtrl.getComplaints);

router.get("/getContact", contactCtrl.getContact);

router.get("/getsocialLink", socialCtrl.getsocialLink);

router.get("/getsocialFeedsLink", socialFeedCtrl.getsocialFeedsLink);

router.get("/getEnquires", queryCtrl.getEnquires);

router.get("/fetchMeeting", meetingCtrl.fetchMeeting);

router.get("/getFeaturedImage", featuredImageCtrl.getFeaturedImage);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
  passport.authenticate(
    "jwt",
    config.passportOptions,
    (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError(
          "token not matched",
          httpStatus.INTERNAL_SERVER_ERROR
        );
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(
          `token not matched ${info}`,
          httpStatus.UNAUTHORIZED
        );
        return next(err);
      }
    }
  )(req, res, next);
});

export default router;
