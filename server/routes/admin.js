import express from "express";
import validate from "express-validation";
import httpStatus from "http-status";
import passport from "passport";
import paramValidation from "../../config/param-validation";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import multer from "multer";

import userCtrl from "../controllers/user";
import profileCtrl from "../controllers/adminProfile";
import pollCtrl from "../controllers/poll";
import journeyCtrl from "../controllers/journey";
import complaintCtrl from "../controllers/complaints";
import contactCtrl from "../controllers/contact";
import configCtrl from "../controllers/config";
import socialCtrl from "../controllers/socialLinks";
import meetingCtrl from "../controllers/meeting";
import featuredImageCtrl from "../controllers/featuredImage";
import adminRegisterCtrl from "../controllers/adminRegister";
import achievementCtrl from "../controllers/achievement";
import socialFeedCtrl from "../controllers/socialFeed";
import votersCtrl from "../controllers/newVoters";
import twitterCtrl from "../controllers/twitter";

const router = express.Router();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function(req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = "./public/uploads" + imageName;
  }
});
const uploadObj = multer({ storage: storage });

router.post("/adminRegister", adminRegisterCtrl.adminRegister);

router.get("/adminRegister/fetchAdmin", adminRegisterCtrl.fetchAdmin);

router.post("/register", userCtrl.createUser);

router
  .route("/adminProfile")
  .post(uploadObj.single("image"), profileCtrl.profile);

router.post("/poll", pollCtrl.poll);

router.post("/pollStatus", pollCtrl.pollStatus);

router.route("/journey").post(uploadObj.single("image"), journeyCtrl.journey);

router
  .route("/achievement")
  .post(uploadObj.single("image"), achievementCtrl.achievement);

router.post("/complaints", complaintCtrl.complaints);

router.post("/contact", contactCtrl.contact);

router.post("/socialLink", socialCtrl.socialLink);

router.post("/socialFeedsLink", socialFeedCtrl.socialFeedsLink);

router.post("/newVoters", votersCtrl.newVoters);

router.post("/reviewDelete", userCtrl.reviewDelete);

router.post("/fetchConfig", configCtrl.fetchConfig);

router.get("/dashboardComplaintData", complaintCtrl.dashboardComplaintData);

router.post('/twitterFeeds', twitterCtrl.twitterFeeds);

router.post('/youtubeId', twitterCtrl.youtubeId);

router.post("/meeting", meetingCtrl.meeting);

router
  .route("/featuredImage")
  .post(uploadObj.array("image"), featuredImageCtrl.featuredImage);

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use((req, res, next) => {
  passport.authenticate(
    "jwt",
    config.passportOptions,
    (error, userDtls, info) => {
      //eslint-disable-line
      if (error) {
        const err = new APIError("token not matched", httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(
          `token not matched and error msg ${info}`,
          httpStatus.UNAUTHORIZED
        );
        return next(err);
      }
    }
  )(req, res, next);
});

export default router;
