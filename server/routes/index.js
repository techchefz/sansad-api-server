import express from 'express';
import authRoutes from './auth';
import userRoutes from './user';
import adminRoutes from "./admin";
import superAdminRoutes from "./superAdmin";

//import csvToJson from './csvToJson';


const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK'));

router.get('/', (req, res) =>
  res.send('It`s Working'));

// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount admin routes at /admin
router.use('/admin', adminRoutes);

// mount super admin routes at /superadmin
router.use('/superadmin', superAdminRoutes);


export default router;

