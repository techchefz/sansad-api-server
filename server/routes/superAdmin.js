import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import passport from 'passport';
import paramValidation from '../../config/param-validation';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import multer from "multer";

import adminRegisterCtrl from "../controllers/adminRegister";
import userCtrl from "../controllers/user";

const router = express.Router();


router.get('/adminRegister/fetchAdmin', adminRegisterCtrl.fetchAdmin)

router.get('/fetchAllUsers', userCtrl.fetchAllUsers)

router.get('/fetchUserMonthlyCount', userCtrl.fetchUserMonthlyCount)

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      } else if (userDtls) {
        req.user = userDtls;
        next();
      } else {
        const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
        return next(err);
      }
    })(req, res, next);
  });
  
  export default router;
  