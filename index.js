import Promise from 'bluebird';
import mongoose from 'mongoose';
import config from './config/env';
import app from './config/express';

// promisify mongoose
Promise.promisifyAll(mongoose);


// connect to mongo db
mongoose.connect(config.db, { server: { socketOptions: { keepAlive: 1 } } }, () => {
  if (config.env === 'test') {
    //    mongoose.connection.db.dropDatabase();
    console.log("database connected");
  }
});
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${config.db}`);
});


const debug = require('debug')('IHI-Web-Api:index');

// listen on port config.port
app.listen(process.env.PORT || config.port, () => {
  console.log(`Rajneta Server Started On Port : ${config.port} (${config.env})`);

  // debug(`server started on port ${config.port} (${config.env})`);
});

export default app;
