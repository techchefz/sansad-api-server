import Joi from 'joi';

export default {
  // POST /api/users/register
  createUser: {
    body: {
      Name: Joi.string().required(),
      Email: Joi.string().required(),
      password: Joi.string().required(),
      PhoneNo: Joi.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      userType: Joi.string().required()
    }
  },
  reviews: {

  },
  sendOtp: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  findState: {
    body: {
      state: Joi.string().required()
    }
  },

  findCity: {
    body: {
      city: Joi.string().required()
    }
  },
};
